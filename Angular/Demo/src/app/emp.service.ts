import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmpService {

  getAllDepartments(): any {
    return this.http.get('http://localhost:8085/getDepartments');
  }

  getEmployeeById(empId : any): any{
    return this.http.get('http://localhost:8085/getEmployeeById/' + empId);
  }


  getEmployees(): any {
    return this.http.get('http://localhost:8085/getEmployees');
  }

  regsiterEmployee(employee: any): any {
    return this.http.post('http://localhost:8085/addEmployee', employee);
  }
  
  isUserLoggedIn: boolean;

  constructor(private http: HttpClient) { 
    this.isUserLoggedIn = false;
  }

  getAllCountries(): any {
    return this.http.get('https://restcountries.com/v3.1/all');
  }


  //Login
  setIsUserLoggedIn() {
    this.isUserLoggedIn = true;
  }

  getIsUserLogged(): boolean {
    return this.isUserLoggedIn;
  }

  //Logout
  setIsUserLoggedOut() {
    this.isUserLoggedIn = false;
  }

  deleteEmployee(empId: any) {
    return this.http.delete('http://localhost:8085/deleteEmployeeById/' + empId);
  }

  employeeLogin(emailId: any, password: any): any {
    return this.http.get('http://localhost:8085/empLogin/' + emailId + '/' + password).toPromise();
  }

  updateEmployee(employee: any) {
    return this.http.put('http://localhost:8085/updateEmployee', employee);
  }
}

