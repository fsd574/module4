import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductService } from '../product.service';
// import { CartService } from '../cart.service';

@Component({
  selector: 'app-productinfo',
  templateUrl: './productinfo.component.html',
  styleUrls: ['./productinfo.component.css']
})
export class ProductinfoComponent implements OnInit{
  @Input() product: any;
  cartProducts: any;

  
  constructor(private route: ActivatedRoute, private productService: ProductService) { }
  
  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      const productId = params.get('id');
      if (productId !== null) {
        this.product = this.productService.getProductById(productId);
      } else {
        console.error('Product ID is null');
      }
    });
     
  }
  addToCart() {
    this.cartProducts = this.cartProducts || [];
    console.log('Adding to cart:',this.product);

    this.cartProducts.push(this.product);
    localStorage.setItem("cartItems", JSON.stringify(this.cartProducts));
    console.log('Cart after adding:', this.cartProducts);
}
}


