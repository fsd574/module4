import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrl: './test.component.css'
})
export class TestComponent implements OnInit {

  id: number;
  name: string;
  avg: number;

  address: any;
  hobbies: any;
  constructor(){
    // alert("constructor invoked...");
    this.id = 101;
    this.name = 'Sri';
    this.avg = 45.56;

    this.address = {
      streetNo:101,
      city: 'Hyderabad',
      state: 'Telangana'
    };

    this.hobbies = ['Sleeping', 'Studying', 'Eating', 'Music', 'Playing'];
  }
ngOnInit(){
  // alert("ngOnInit invoked...");
}
}
