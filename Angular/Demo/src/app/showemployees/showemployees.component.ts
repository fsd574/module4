import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';

declare var jQuery: any;

@Component({
  selector: 'app-showemployees',
  templateUrl: './showemployees.component.html',
  styleUrl: './showemployees.component.css'
})
export class ShowemployeesComponent implements OnInit{
  
  employees: any;
  emailId: any;
  countries: any;
  departments: any;
  editEmp: any;

   //Dependency Injection for EmpService
   constructor(private service: EmpService) {
    this.emailId = localStorage.getItem("emailId");

   //for 2-way databinding with dialog box
   this.editEmp = {
    empId: '',
    empName: '',
    salary: '',
    gender: '',
    doj: '',
    country: '',
    emailId: '',
    password: '',
    department: {
      deptId: ''
    }
  };
}


    //Getting emailId from LocalStorage
    // this.emailId = localStorage.getItem('emailId');

    // this.employees = [
    //   {empId: 101, empName:'Harsha', salary:1212.12, gender:'Male',   doj:'2018-11-15', country:'India',    emailId:'harsha@gmail.com', password:'123'},
    //   {empId: 102, empName:'Pasha',  salary:2323.23, gender:'Male',   doj:'2017-10-16', country:'China',    emailId:'pasha@gmail.com',  password:'123'},
    //   {empId: 103, empName:'Indira', salary:3434.34, gender:'Female', doj:'2016-09-17', country:'USA',      emailId:'indira@gmail.com', password:'123'},
    //   {empId: 104, empName:'Vamsi',  salary:4545.45, gender:'Male',   doj:'2015-08-18', country:'SriLanka', emailId:'vamsi@gmail.com',  password:'123'},
    //   {empId: 105, empName:'Venkat', salary:5656.56, gender:'Male',   doj:'2014-07-19', country:'Nepal',    emailId:'venkat@gmail.com', password:'123'}
    // ];
  
    ngOnInit() {
      this.service.getEmployees().subscribe((data: any) => {
        console.log(data);
        this.employees = data;
      });
      this.service.getAllCountries().subscribe((data: any) => { this.countries = data; });
      this.service.getAllDepartments().subscribe((data: any) => { this.departments = data; });
    }
    


submit(){
  console.log(this.employees)
}

editEmployee(emp: any) {
  console.log(emp);

  //2. Launching the Modal Dialog Box
  jQuery('#myModal').modal('show');

}

updateEmployee() {
  console.log(this.editEmp);
  this.service.updateEmployee(this.editEmp).subscribe((data: any) => { console.log(data); });
}

deleteEmployee(emp: any) {
  this.service.deleteEmployee(emp.empId).subscribe((data: any) => {console.log(data);});

  const i = this.employees.findIndex((element: any) => {
    return element.empId == emp.empId;
  });

  this.employees.splice(i, 1);

  alert('Employee Deleted Successfully!!!');
}
}

