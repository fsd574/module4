import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  products: any;
  emailId: any;
  cartProducts: any;
  selectedProduct: any;

  constructor() {
  this.emailId = localStorage.getItem('emailId');
  this.cartProducts = [];

  this.products = [
    {
      id: 101,
      name: 'Duck',
      description: 'No Cost EMI Applicable',
      price: '100',
      imageUrl: 'assets/images/toy1.jpg'
      // imageUrl: 'https://tse4.mm.bing.net/th?id=OIP.IfuRgOyVWuOKlfMle37gGQHaIE&pid=Api&P=0&h=180'
    },
    {
      id: 102,
      name: 'SpiderMan',
      description: 'No Cost EMI Applicable',
      price: '200',
      imageUrl: 'assets/images/toy.2.jpg'
      // imageUrl: 'https://tse4.mm.bing.net/th?id=OIP.41juCgHHp_SQblcecIG2JgHaHa&pid=Api&P=0&h=180'
    },
    {
      id: 103,
      name: 'Robot',
      description: 'No Cost EMI Applicable',
      price: '300', 
      imageUrl: 'assets/images/toy.3.jpg'
      // imageUrl: 'https://tse3.mm.bing.net/th?id=OIP.F1xJgH1Mfooe0pQi7V17IgHaHa&pid=Api&P=0&h=180'

    },
    {
      id: 104,
      name: 'Doggie',
      description: 'No Cost EMI Applicable',
      price: '400',  
      imageUrl: 'assets/images/toy4.jpg'
      // imageUrl: 'https://tse3.mm.bing.net/th?id=OIP.WIF1UhwIO7hY6ewHVuGhfAHaFa&pid=Api&P=0&h=180'
    },
    {
      id: 105,
      name: 'Teddy',
      description: 'No Cost EMI Applicable',
      price: '2000',
      imageUrl: 'assets/images/toy5.jpg'
      // imageUrl: 'https://tse4.explicit.bing.net/th?id=OIP.ZdQe3mtR0atNMEzwUfED9AHaGK&pid=Api&P=0&h=180'
    },
    {
      id: 106,
      name: 'Piano',
      description: 'No Cost EMI Applicable',
      price: '790',
      imageUrl: 'assets/images/toy6.jpg'
      // imageUrl: 'https://tse3.mm.bing.net/th?id=OIP.Xic0bnDyrFle8axIZ_xIuQHaFK&pid=Api&P=0&h=180'
    },
  ];

  }

   ngOnInit() {    
  }

  addToCart(product: any) {
    this.cartProducts.push(product);
    localStorage.setItem("cartItems", JSON.stringify(this.cartProducts));
  }
  selectProduct(product: any) {
    this.selectedProduct = product;
  }
}


