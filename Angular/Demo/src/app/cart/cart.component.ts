import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrl: './cart.component.css'
})
export class CartComponent implements OnInit {
  emailId: any;
  products: any;
  localStorageData: any;

  constructor() {
    this.emailId = localStorage.getItem('emailId');
    this.localStorageData = localStorage.getItem('cartItems');
    this.products = JSON.parse(this.localStorageData);
    console.log(this.products);
  }

  ngOnInit() {
  }
  onPurchase() {
    
    localStorage.removeItem('cartItems');
    this.products = [];
    
  }
  onDelete(index: number) {
    this.products.splice(index, 1);
    this.calculateTotal();
  }

  calculateTotal(): number {
    // Calculate the total based on product prices
    return this.products.reduce((total:any, product:any) => total + (product.price || 0), 0);
  }

}
