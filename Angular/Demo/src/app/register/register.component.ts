
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  countries: any;
  departments: any;
  emp: any;
  captchaResponse: string = '';
  password:string='';
  confirmPassword: string = '';
  emailId:string ='';
  phoneNumber:string='';
  doj:string='';
  gender:string='';
  salary:string='';
  empName:string='';
  isSubmitted: boolean = false;

  constructor(private router: Router, private service: EmpService, private toastr: ToastrService) {
    this.emp = {
      empName:'',
      salary:'',
      gender:'',
      doj:'',
      country:'',
      emailId:'',
      password:'',
      department: {
        deptId:''
      }
    };
  }

  ngOnInit() {
    this.service.getAllCountries().subscribe((data: any) => {this.countries = data;});
    this.service.getAllDepartments().subscribe((data: any) => {this.departments = data;});
  }
 

  registerSubmit(regForm: any) {
    console.log('Register form submitted:', regForm);

    this.isSubmitted = true;

    const trimmedPassword = this.emp.password.trim();  
    const trimmedConfirmPassword = this.confirmPassword.trim();  

    console.log('Passwords (trimmed):', this.emp.password, this.confirmPassword);  
    console.log('Password lengths:', this.emp.password.length, this.confirmPassword.length);  

    if (this.emp.password.length > 0 && this.emp.password === this.confirmPassword) {  
        // Rest of your code remains unchanged
    } else {
        // Passwords do not match
        this.toastr.error('Passwords do not match.', 'Error', {
            closeButton: true,
            progressBar: true,
            positionClass: 'toast-top-right',
            tapToDismiss: false,
            timeOut: 3000, // 3 seconds
        });
    }
    this.emp.empName = regForm.empName;
    this.emp.salary = regForm.salary;
    this.emp.gender = regForm.gender;
    this.emp.doj = regForm.doj;
    this.emp.country = regForm.country;
    this.emp.emailId = regForm.emailId;
    this.emp.password = regForm.password;
    this.emp.department.deptId = regForm.department;

    // console.log(this.emp);

    this.service.regsiterEmployee(this.emp).subscribe((data: any) => {console.log(data);});

    // if (!this.validateForm(regForm)) {
    //   return;
    // }
    
    this.toastr.success('Registration Successful!', 'Success', {
      closeButton: true,
      progressBar: true,
      positionClass: 'toast-top-right',
      tapToDismiss: false,
      timeOut: 3000, // 3 seconds
    });

    this.router.navigate(['login']);
  }

  private validateForm(regForm: any): boolean {

    if (!regForm.empName || !regForm.salary || !regForm.emailId || !regForm.password) {
      this.toastr.error('Please fill in all required fields.', 'Error', {
        closeButton: true,
        progressBar: true,
        positionClass: 'toast-top-right',
        tapToDismiss: false,
        timeOut: 3000, // 3 seconds
      });
      return false;
    }
    return true;
  }
  private passwordsMatch(password1: string, password2: string): boolean {
    const trimmedPassword1 = password1.trim();
    const trimmedPassword2 = password2.trim();

    return trimmedPassword1 === trimmedPassword2;
}
  handleCaptchaResponse(captchaResponse: string) {
    this.captchaResponse = captchaResponse;
  }
}