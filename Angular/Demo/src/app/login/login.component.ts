import { Component, OnInit } from '@angular/core';

//Import Router class
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent implements OnInit {

  emp: any;
  captchaResponse: string = '';
  employees: any;
  emailId: any;
  password: any;

  //Dependency Injection for EmpService, Router
  constructor(private router: Router, private toastr: ToastrService, private service: EmpService) {   
  }

  ngOnInit(){
  }

  submit(regForm: any) {
    console.log("EmailId : " + this.emailId);
    console.log("Password: " + this.password);
    console.log(regForm);

  
  if (!this.captchaResponse) {
    alert('Please complete the captcha.');
    return;
  }
}

  loginSubmit(loginForm: any) {
    console.log(loginForm);
    console.log("EmailId : " + loginForm.emailId);
    console.log("Password: " + loginForm.password);

    if (loginForm.emailId == 'HR' && loginForm.password == 'HR') {
      //Setting the isUserLoggedIn variable value to true under EmpService
      this.service.setIsUserLoggedIn();

      localStorage.setItem("emailId", loginForm.emailId);
      this.router.navigate(['showemps']);
    } else {
      this.employees.forEach((element: any) => {
        if (element.emailId == loginForm.emailId && element.password == loginForm.password) {
          this.emp = element;
        }
      });

      if (this.emp != null) {
        //Setting the isUserLoggedIn variable value to true under EmpService
        this.service.setIsUserLoggedIn();

        localStorage.setItem("emailId", loginForm.emailId);
        this.router.navigate(['product']);
      } else {

        this.toastr.error('Invalid Credentials', 'Error', {
          closeButton: true,
          progressBar: true,
          positionClass: 'toast-top-right',
          tapToDismiss: false,
          timeOut: 3000, // 3 seconds
        });
        return;
      }
    }
    this.toastr.success('Login Successful!', 'Success', {
      closeButton: true,
      progressBar: true,
      positionClass: 'toast-top-right',
      tapToDismiss: false,
      timeOut: 3000, // 3 seconds
    });
  }
  handleCaptchaResponse(captchaResponse: string) {
    this.captchaResponse = captchaResponse;
  }
}

//   async loginSubmit(loginForm: any) {
//     if (loginForm.emailId == 'HR' && loginForm.password == 'HR') {           
//       //Setting the isUserLoggedIn variable value to true under EmpService
//       this.service.setIsUserLoggedIn();
//       localStorage.setItem("emailId", loginForm.emailId);
//       this.router.navigate(['showemps']);
//     } else {
//       this.emp = null;

//       await this.service.employeeLogin(loginForm.emailId, loginForm.password).then((data: any) => {
//         console.log(data);
//         this.emp = data;
//       });

//       if (this.emp != null) {
//         this.service.setIsUserLoggedIn();        
//         localStorage.setItem("emailId", loginForm.emailId);
//         this.router.navigate(['products']);
//       } else {
//         alert('Invalid Credentials');
//       }
//     }
//   }
// }


