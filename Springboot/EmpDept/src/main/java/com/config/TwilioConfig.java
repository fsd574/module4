package com.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.twilio.Twilio;

@Configuration
	public class TwilioConfig {

	    @Value("${twilio.accountSid}")
	    private String accountSid;

	    @Value("${twilio.authToken}")
	    private String authToken;

	    @Bean
	    public void twilioInitializer() {
	        Twilio.init(accountSid, authToken);
	    }
	}






//package com.service;
//
//import com.twilio.Twilio;
//import com.twilio.rest.api.v2010.account.Message;
//import com.twilio.type.PhoneNumber;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.stereotype.Service;
//
//import com.twilio.Twilio;
//
//@Service
//public class TwilioSMSService {
//	
//    @Value("${twilio.account.sid}")
//    private String accountSid;
//
//    @Value("${twilio.auth.token}")
//    private String authToken;
//
//    
//    @Value("${twilio.phone.number}")
//    private String twilioPhoneNumber;
//
//    public void sendThankYouMessage(String registeredMobileNumber) {
//        
//        Twilio.init(accountSid, authToken);
//
//        PhoneNumber to = new PhoneNumber(registeredMobileNumber);
//
//        // Message body
//        String thankYouMessage = "Thank you for registering with our service!";
//
//        try {
//            // Send SMS
//            Message message = Message.creator(to, new PhoneNumber(twilioPhoneNumber), thankYouMessage).create();
//
//            // Log the message SID or handle success as needed
//            System.out.println("Message SID: " + message.getSid());
//        } catch (Exception e) {
//            // Handle Twilio API exceptions or other errors
//            e.printStackTrace();
//        }
//    }
//}


