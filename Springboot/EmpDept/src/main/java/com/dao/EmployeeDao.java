package com.dao;

import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.model.Employee;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

@Service
public class EmployeeDao {

    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private EmployeeRepository employeeRepository;

    private final String twilioAccountSid = "ACbf62c9dc61fca8c326f2bd62c1c310f1 ";
	private final String twilioAuthToken = "8150a4a4a44050b2e7888ad62f6cdfc8";
	private final String twilioPhoneNumber = "+15072644767";

	static {
	     Twilio.init("ACbf62c9dc61fca8c326f2bd62c1c310f1", "8150a4a4a44050b2e7888ad62f6cdfc8");
	}
    public List<Employee> getEmployees() {
        return employeeRepository.findAll();
    }

    public Employee getEmployeeById(int employeeId) {
        return employeeRepository.findById(employeeId).orElse(null);
    }

    public Employee getEmployeeByEmail(String emailId) {
        return employeeRepository.findByEmailId(emailId);
    }

    public Employee addEmployee(Employee employee) {
        BCryptPasswordEncoder bcrypt = new BCryptPasswordEncoder();
        String encryptedPwd = bcrypt.encode(employee.getPassword());
        employee.setPassword(encryptedPwd);

        employee = employeeRepository.save(employee);

        sendThankYouEmail(employee.getEmailId());
        sendThankYouSMS(employee.getMobileNumber());

        return employee;
    }

    private void sendThankYouSMS(String mobileNumber) {
        try {
            Message message = Message.creator(
                    new PhoneNumber(mobileNumber),
                    new PhoneNumber(twilioPhoneNumber),
                    generateRandomOTP()
            ).create();
            System.out.println("SMS sent successfully. SID: " + message.getSid());
        } catch (Exception e) {
            System.err.println("Error sending SMS: " + e.getMessage());
            e.printStackTrace();
        }
    }

    private String generateRandomOTP() {
        int otpLength = 6;
        String numbers = "0123456789";
        StringBuilder otp = new StringBuilder(otpLength);
        Random random = new Random();
        for (int i = 0; i < otpLength; i++) {
            int index = random.nextInt(numbers.length());
            otp.append(numbers.charAt(index));
        }
        return otp.toString();
    }

    private void sendThankYouEmail(String emailId) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(emailId);
        message.setSubject("Thank You for Registering");
        message.setText("Dear User,\n\nThank you for registering with our application.");

        javaMailSender.send(message);
    }

    public Employee updateEmployee(Employee employee) {
        return employeeRepository.save(employee);
    }

    public void deleteEmployeeById(int employeeId) {
        employeeRepository.deleteById(employeeId);
    }

	public Employee getEmployeeByName(String employeeName) {
		return employeeRepository.findByName(employeeName);
    }

	public Employee employeeLogin(String emailId, String password) {
	    // Find the employee by emailId
	    Employee employee = employeeRepository.findByEmailId(emailId);

	    // Check if employee exists and the provided password matches the stored password
	    if (employee != null) {
	        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
	        if (passwordEncoder.matches(password, employee.getPassword())) {
	            // Passwords match, return the employee
	            return employee;
	        }
	    }

	    // Return null if login fails
	    return null;
	}

}
